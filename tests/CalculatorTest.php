<?php

namespace Tests\App;

use App\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{

    public function testSum()
    {
        $calculator = new Calculator();
        $this->assertSame(0, $calculator->sum(0, 0));
        $this->assertSame(99, $calculator->sum(50, 49));
        $this->assertSame(-99, $calculator->sum(-100, 1));
    }

}
